# The "Sorken" Gopher Browser

"Sorken" is a browser for the gopherspace, a network of hundreds of
servers that serve ad-free content in an easy-to-consume format.

Mozilla used to have a Gopher client called Firefox, but they stopped
development. This client is a modern alternative. There are other
clients if you look around.

## Current status

Working but missing some essential features.

## Usage

* Clone the repository
* Install [Akku.scm](https://akkuscm.org)
* Run `akku install` in the repository
* (Chez Scheme) Run `.akku/env scheme --program bin/sorken.sps`
* (Loko Scheme) Run `.akku/env loko -feval --compile bin/sorken.sps --output bin/sorken; .akku/env bin/sorken`

Run in Kitty or another terminal emulator that supports emojis.

## Screenshot

![Screenshot of sorken](sorken-quux.png)
