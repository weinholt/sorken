#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2020 Göran Weinholt
;; SPDX-License-Identifier: GPL-3.0-or-later
#!r6rs

(import
  (rnrs (6))
  (sorken gopher client)
  (sorken gopher directories)

  (text-mode console)
  (text-mode console events)

  (prefix (text-mode imtui id) ui:)
  (prefix (text-mode imtui) ui:)
  (prefix (text-mode imtui boxes) ui:)
  (prefix (text-mode imtui list-views) ui:)
  (prefix (text-mode imtui sliders) ui:))

(define ui (ui:make-context))

(define-record-type document
  (fields id slider-id list-id
          (mutable dirent)
          title
          border-color
          rect
          (mutable body)
          (mutable selected-item)
          (mutable history)))

(define (read-menu-file fn)
  (call-with-port (open-file-input-port fn (file-options)
                                        (buffer-mode block)
                                        (make-transcoder (utf-8-codec)
                                                         (eol-style crlf)))
    (lambda (p)
      (let lp ((dirents '()))
        (let ((line (get-line p)))
          (if (or (eof-object? line) (string=? line "."))
              (list->vector (reverse dirents))
              (let ((d (parse-dirent line)))
                (if (dirent? d)
                    (lp (cons d dirents))
                    (lp dirents)))))))))

(define (new-document dirent)
  (make-document (ui:next-imtui-id) (ui:next-imtui-id) (ui:next-imtui-id)
                 dirent
                 #f
                 LightGreen
                 (ui:make-rect 0 0 (window-width) (window-height))
                 (gopher-menu-transaction dirent)
                 0
                 (list dirent)))

(define documents
  (list
   (if (null? (cdr (command-line)))
       (new-document (url->dirent "gopher.quux.org"))
       (new-document (url->dirent (cadr (command-line)))))))

(define (current-document)
  (cond ((memp (lambda (doc)
                  (ui:id=? (document-list-id doc) (ui:context-keyboard-item ui)))
               documents)
         => car)
        (else #f)))

(define (render-dirent doc d selected?)
  (cond (selected?
         (text-background (document-border-color doc))
         (text-color Black))
        (else
         (text-background Default)
         (text-color Default)))
  (case (dirent-type d)
    [(#\0) (print "📃") (text-attribute 'underline #t)]
    [(#\1) (print "📂") (text-attribute 'underline #t)]
    [(#\3) (print "📢") (text-color LightRed)]
    [(#\5) (print "💾") (text-attribute 'underline #t)]  ; or 📦
    [(#\7) (print "🔦")]  ; or 🔍
    [(#\9) (print "📎") (text-attribute 'underline #t)]
    [(#\g) (print "🎨") (text-attribute 'underline #t)]
    [(#\I) (print "📷") (text-attribute 'underline #t)]
    ;; Not from RFCs
    [(#\i) (print "  ")]                ;info?
    [(#\h) (print "⚓") (text-attribute 'underline #t)]
    [(#\d) (print "📒") (text-attribute 'underline #t)]
    [(#\s) (print "🎜") (text-attribute 'underline #t)]
    ((#\;) (print "🎥") (text-attribute 'underline #t)) ;or 🎬
    [(#\M) (print "📧" ) (text-attribute 'underline #t)]
    (else
     (let* ((t (dirent-type d))
            (t (cond ((symbol? t) (string-ref (symbol->string t) 0))
                     ((number? t) (string-ref (number->string t) 0))
                     (else t))))
       (cond ((char<=? #\! t #\~)
              ;; Print unknown stuff in fullwidth, e.g. W -> Ｗ
              (print (string (integer->char (fx+ #xFF00 (fx- (char->integer t)
                                                             (char->integer #\space)))))))
             (else
              (print (dirent-type d))
              (print " "))))))
  (print (dirent-name d))
  (text-attribute 'underline #f)
  (text-color Default)
  (when (and (not (eqv? (dirent-type d) #\i))
             (let ((where (document-dirent doc)))
               (not (and (equal? (dirent-server where) (dirent-server d))
                         (eqv? (dirent-port where) (dirent-port d))))))
    (print "🔗"))
  (when (dirent-url? d)                 ;external URL
    (text-color LightRed)
    (print "🡕")
    (text-color Default))
  (when (dirent-gopher+? d)
    (text-color DarkGray)
    (print "+")
    (text-color Default))
  (clreol)
  (text-background Default)
  (text-color Default)
  (case (dirent-type d)
    [(#\7)
     ;; FIXME: Make this into a real thing
     (gotoxy (fx+ (wherex) 1) (wherey))
     (print "[_________________]")]))

(define (render-document doc)
  (let* ((body (document-body doc))
         (selected-item (document-selected-item doc)))

    (case (dirent-type (document-dirent doc))
      [(#\0)
       (ui:list-view ui (document-list-id doc)
                     (ui:make-rect 0 0 (fx- (window-width) 2) (window-height))
                     (vector-length body)
                     (ui:ref selected-item)
                     (lambda (i selected?)
                       (cond (selected?
                              (text-background (document-border-color doc))
                              (text-color Black))
                             (else
                              (text-background Default)
                              (text-color Default)))
                       (print (vector-ref body i))
                       (clreol)
                       (text-background Default)
                       (text-color Default)))
       ;; TODO: switch between pages

       ]
      [(#\1)
       (when (ui:list-view ui (document-list-id doc)
                           (ui:make-rect 0 0 (fx- (window-width) 2) (window-height))
                           (vector-length body)
                           (ui:ref selected-item)
                           (lambda (i selected?)
                             (let ((d (vector-ref body i)))
                               (render-dirent doc d selected?))))
         (let ((item (vector-ref body selected-item)))
           ;; FIXME: save the navigation history
           ;; FIXME: transactions should take a callback
           (case (dirent-type item)
             [(#\0)
              (document-dirent-set! doc item)
              (document-history-set! doc (cons item (document-history doc)))
              (document-body-set! doc (gopher-textfile-transaction item))
              (set! selected-item 0)
              (ui:context-fixpoint?-set! ui #f)]
             [(#\1)
              (document-dirent-set! doc item)
              (document-history-set! doc (cons item (document-history doc)))
              (document-body-set! doc (gopher-menu-transaction item))
              (set! selected-item 0)
              (ui:context-fixpoint?-set! ui #f)])))]
      [else
       (print "No renderer for type ")
       (print (dirent-type (document-dirent doc)))])

    (when (ui:slider ui (document-slider-id doc)
                     (ui:make-rect (- (window-width) 1) 0 1 (window-height))
                     (document-border-color doc)
                     Default
                     0 (- (vector-length (document-body doc)) 1)
                     (ui:ref selected-item))
      ;; (document-selected-item-set! doc (round selected-item))
      (ui:context-fixpoint?-set! ui #f))

    (document-selected-item-set! doc (round selected-item))))

(let lp ()
  (ui:prepare ui)

  (for-each
   (lambda (doc)
     ;; XXX: title/document might be too long, text-mode should truncate it
     (ui:box ui (document-id doc) (document-rect doc)
             (or (document-title doc) (dirent->url (document-dirent doc)))
             (document-border-color doc)
             (lambda ()
               (text-color Default)
               (render-document doc)))
     ;; FIXME: Fix text-mode because this should not be necessary
     (text-color Default)
     (text-background Default)
     )
   documents)

  #;
  (ui:box ui ui:genid (ui:genrect 1 10 (div (window-width) 2) 14 2) "UI state" Yellow
          (lambda ()
            (print "mouse down event: ") (println (ui:context-mouse-down-event ui))
            (print "mouse event:      ") (println (ui:context-mouse-event ui))
            (print "hot item:         ") (println (ui:context-hot-item ui))
            (print "active item:      ") (println (ui:context-active-item ui))
            (print "active data:      ") (println (ui:context-active-data ui))
            (print "keyboard item:    ") (println (ui:context-keyboard-item ui))
            (print "last-item:        ") (println (ui:context-last-item ui))
            (print "mouse-down?:      ") (println (ui:context-mouse-down? ui))
            (print "mouse-attributes: ") (println (ui:context-mouse-attributes ui))
            (print "keyboard-event:   ") (println (ui:context-keyboard-event ui))))

  (ui:finish ui)

  (cond
    ((not (ui:context-fixpoint? ui))
     (lp))
    (else
     (update-screen)
     (let ((ev (read-event)))
       (unless (or (input-closed-event? ev)
                   (and (key-press-event? ev)
                        (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                        (eqv? (keyboard-event-key ev) #\q)))
         (cond
           ((resize-event? ev)
            (redraw-screen))
           ((mouse-event? ev)
            (ui:handle-mouse-event ui ev))

           ((and (key-press-event? ev)
                 (and (enum-set=? (keyboard-event-mods ev) (modifier-set ctrl))
                      (eqv? (keyboard-event-key ev) #\l)))
            (resize-screen))

           ((and (key-press-event? ev)
                 (and (enum-set=? (keyboard-event-mods ev) (modifier-set))
                      (eqv? (keyboard-event-key ev) #\h)))
            (cond
              ((current-document) =>
               (lambda (doc)
                 (document-dirent-set! doc (parse-dirent "1History\t/history\thost.invalid\t70"))
                 (document-body-set! doc (list->vector (document-history doc)))
                 (document-selected-item-set! doc 0)))))

           ((key-press-event? ev)
            (ui:handle-keyboard-event ui ev)))

         (lp))))))

(restore-console)
