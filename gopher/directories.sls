;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2020 Göran Weinholt
;; SPDX-License-Identifier: GPL-3.0-or-later
#!r6rs

(library (sorken gopher directories)
  (export
    make-dirent dirent?
    dirent-type
    dirent-name
    dirent-selector
    dirent-server
    dirent-port
    dirent-gopher+?
    dirent-url?

    dirent-url
    dirent->url
    url->dirent

    parse-dirent
    )
  (import
    (rnrs))

(define (string-split str c max)
  (let lp ((start 0) (end 0) (max max))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (if (eqv? max 0)
               (list (substring str start (string-length str)))
               (cons (substring str start end)
                     (lp (fx+ end 1) (fx+ end 1) (fx- max 1)))))
          (else
           (lp start (fx+ end 1) max)))))

;; Directory entity
(define-record-type dirent
  (sealed #t)
  (fields type
          name
          selector                      ;FIXME: decide if this can contain a search query
          server
          port                          ;FIXME: convention adds 100000 for TLS?
          gopher+?))

(define (dirent-url? d)
  (let ((sel (dirent-selector d)))
    (and (>= (string-length sel) (string-length "URL:"))
         (string=? "URL:" (substring sel 0 (string-length "URL:"))))))

(define (dirent-url d)
  (if (dirent-url? d)
      (let ((sel (dirent-selector d)))
        (substring sel (string-length "URL:") (string-length sel)))
      (dirent->url d)))

(define (dirent->url d)
  ;; FIXME: Verify this with rfc4266
  ;; FIXME: URI or URL?
  (string-append "gopher://"
                 (dirent-server d)
                 ":"
                 (number->string (dirent-port d))
                 "/"
                 (string (dirent-type d))
                 (dirent-selector d)))

(define (generic-url->dirent url)
  ;; gopher://gopher.quux.org:70/0/Archives/Mailing Lists/gopher/gopher.2002-02|/MBOX-MESSAGE/34

  ;; XXX: the server should be ready to serve a redirect, so this
  ;; should really take a selector for where it appeared...
  (make-dirent #\h
               url
               (string-append "URL:" url)
               "none.host"
               70
               #f))

(define (url->dirent url)
  ;; TODO: don't forget URL unquoting
  (let ((xs (string-split url #\/ 3)))
    (cond
      ((null? (cdr xs))
       (url->dirent (string-append "gopher://" url)))
      ((or (< (length xs) 3)
           (not (string-ci=? "gopher:" (car xs)))
           (not (string=? "" (cadr xs))))
       (generic-url->dirent url))
      (else
       (let ((xs (cddr xs)))
         (let ((server+port (string-split (car xs) #\: 1))
               (type+selector* (cdr xs)))
           (let-values ([(server port)
                         (cond ((null? (cdr server+port))
                                (values (car server+port) 70))
                               (else
                                (values (car server+port)
                                        (string->number (cadr server+port)))))])
             (if (not port)
                 (generic-url->dirent url)
                 (if (null? type+selector*)
                     (make-dirent #\1 url "" server port #f)
                     (let ((type (string-ref (car type+selector*) 0))
                           (selector (substring (car type+selector*) 1
                                                (string-length (car type+selector*)))))
                       (make-dirent type url selector server port #f)))))))))))

(define (parse-dirent line)
  (cond
    ((equal? line ".")
     (eof-object))
    ((equal? line "")
     #f)
    (else
     (let ((xs (string-split line #\tab -1)))
       (cond
         ((equal? (car xs) "") #f)
         ((< (length xs) 4) #f)
         (else
          (let ((type+name (car xs))
                (selector (cadr xs))
                (server (caddr xs))
                (port (string->number (cadddr xs) 10)))
            (let ((type (string-ref type+name 0))
                  (name (substring type+name 1 (string-length type+name)))
                  (extra (cddddr xs)))
              (let ((gopher+ (and (pair? extra) (equal? (car extra) "+"))))
                (and port
                     (make-dirent type name selector server port gopher+))))))))))))
