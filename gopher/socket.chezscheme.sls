(library (srfi :106 socket)
  (export socket?
          make-client-socket

          socket-input-port
          socket-output-port

          socket-send
          socket-recv
          socket-close
          socket-shutdown

          call-with-socket)
  (import (usocket srfi)))
