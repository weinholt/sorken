;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2020 Göran Weinholt
;; SPDX-License-Identifier: GPL-3.0-or-later
#!r6rs

(library (sorken gopher client)
  (export
    gopher-textfile-transaction
    ;; gopher-binary-file-transaction
    gopher-menu-transaction
    ;; gopher-fulltext-search-transaction
    ;; gopher+-transaction
    )
  (import
    (rnrs)
    (srfi :98 os-environment-variables)
    (srfi :106 socket)
    (sorken gopher directories))

#;
(define (make-cache)
  ;; XXX: should be a persistently stored thing
  (make-hashtable dirent=? dirent-hash))

(define (socket-textual-input-port sock)
  (transcoded-port (socket-input-port sock)
                   (make-transcoder (utf-8-codec) (eol-style crlf))))

(define (read-textfile p)
  (let lp ((dirents '()))
    (let ((line (get-line p)))
      (if (or (eof-object? line) (string=? line "."))
          (list->vector (reverse dirents))
          (let ((line (if (and (fx>? (string-length line) 0)
                               (eqv? (string-ref line 0) #\.))
                          (substring line 1 (string-length line))
                          line)))
            (lp (cons line dirents)))))))

(define (gopher-textfile-transaction d)
  (call-with-socket (make-client-socket (dirent-server d)
                                        (number->string (dirent-port d)))
    (lambda (sock)
      (socket-send sock (string->utf8 (string-append (dirent-selector d) "\r\n")))
      ;; XXX: for niceness this should send lines over a channel
      (read-textfile (socket-textual-input-port sock)))))

(define (read-menu p)
  (let lp ((dirents '()))
    (let ((line (get-line p)))
      (if (or (eof-object? line) (string=? line "."))
          (list->vector (reverse dirents))
          (let ((d (parse-dirent line)))
            (if (dirent? d)
                (lp (cons d dirents))
                (lp dirents)))))))

(define (gopher-menu-transaction d)
  (call-with-socket (make-client-socket (dirent-server d)
                                        (number->string (dirent-port d)))
    (lambda (sock)
      (socket-send sock (string->utf8 (string-append (dirent-selector d) "\r\n")))
      ;; XXX: for niceness this should send menu entries over a channel
      (read-menu (socket-textual-input-port sock)))))

)
